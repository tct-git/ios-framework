// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

import PackageDescription

let package = Package(
    name: "WeFiLibs",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "WeFiLibs",
            targets: ["WeFiLibs"]),
    ],
    targets: [
        .binaryTarget(
            name: "WeFiLibs",
            path: "./Sources/WeFiLibs.xcframework"
        )
    ]
)
