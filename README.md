# WeFiLIbs

========================

__WeFiLibs__ is a framework which allows you to connect to WiFi Amenety and Premium Networks

- __Requirements__: iOS 13 or above
- __Swift version__: Swift 5.5 or above


## Usage

Mainly check the WeFiLibDemo


## Installation

### Swift Package Manager

1. Open your project with __Xcode__  `File -> Swift Packages -> Add package dependency` type your repository url. Go through all the steps. 
3. Go to __General__ pane of the application target in your project to the __Embedded Binaries__ section. Make sure  `WeFiLibs` is there. If not then add it. 
4. Go to your __Signing & Capabilities__ section of your project settings. Add such capabilities: `Access WiFi information`,`Hotspot Configuration`,`Wireless accessory configuration`, `Network Extensions`: with App Proxy, Content Filter, Packet tunel, DNS proxy, `Background Modes`: with Location updates, Background Fetch
5. Go to __Info__ section - edit your Info.plist file. Add `Privacy - Location Always Usage Description - We provide WiFi profiles based on your location`, `Privacy - Location Always and When In Use Usage Description - We provide WiFi profiles based on your location updates`, `Privacy - Location When In Use Usage Description -We provide WiFi profiles based on your location`,  
     `Permitted background task scheduler identifiers` add item - `com.wefi.backgroundtaskidentifier`
     `App Transport Security Settings`- `Allow Arbitrary Loads` to YES
6. Open `AppDelegate` of your project.  Add `import WeFiLibs`. Make it inherited from `WeFiDelegate` like this:
```
import WeFiLibs

@main
class AppDelegate: WeFiDelegate

```
7. In any place you want the sdk start (user will be asked for location permission with this method firing) add `WeFiClient.shared.start()`(or `WeFiProfiles.shared.start()`) like this: 

```
class ViewController: UIViewController {

    override func viewDidLoad() {
    super.viewDidAppear(animated)
    
    WeFiProfiles.shared.locationPermissionDelegate = self
    WeFiProfiles.shared.profilesDelegate = self
    WeFiProfiles.shared.openRoamingProfilesDelegate = self
    WeFiClient.shared.networkParamsDelegate = self
    
    WeFiClient.shared.start()
}
```
8. Test internet parameters
First of all you need to implement 
__NetworkParamsDelegate__
```
protocol NetworkParamsDelegate: AnyObject {
    
    func pingTestResult( _ result: Double?, error: WeFiError?)
    func uploadTestResult(_ result: Double?, error: WeFiError?)
    func downloadTestResult(_ result: Double?, error: WeFiError?)
}
```
You can run tests with these methods: 
```
    WeFiClient.shared.startPingTest()
    WeFiClient.shared.startDownloadTest()
    WeFiClient.shared.startUploadTest()
```
Ping measured in ms, the download and upload tests are in Mbps.
The results will be in appropriate delegates.
You can cancel download/upload processes with these: 
```
    WeFiClient.shared.stopPingTest()
    WeFiClient.shared.cancelUploadTest()
    WeFiClient.shared.cancelDownloadTest()
``` 

9. Get wifi and open roaming profiles.
First of all you need to implement:
__ProfilesDelegate__, __OpenRoamingProfilesDelegate__

```
extension ViewController: OpenRoamingProfilesDelegate {
    
  func openRoamingProfilesDownloadedWithError(_ error: WeFiError?) {
      // You can install now        
  }
    
  func newOpenRoamingProfilesAvailable() {
      //Notified when new version is available
  }
}

extension ViewController: ProfilesDelegate {
    
  func wifiProfilesDownloaded(_ savedUrl: URL?, error: WeFiError?) {
      // You can install now   
  }
    
  func newWiFiProfilesAvailable(_ savedUrl: URL) {
      //Notified when new version is available
  }
}
```
Then check for possibility with `areProfilesReadyForDownload` and try like these: 
```
  if WeFiProfiles.shared.areProfilesReadyForDownload {
      startLoading()
      WeFiProfiles.shared.downloadAndInstallOpenRoamingProfiles()
  }
  if WeFiProfiles.shared.areProfilesReadyForDownload {
      startLoading()
      WeFiProfiles.shared.downloaWiFiProfiles()
  }
```
10. Location delegate to inform you about user's decision. Not required 
```
extension ViewController: LocationPermissionDelegate {
    
  func locationPermissionReceived(_ isEnabled: Bool) {
      //Check if location state is important for your code
  }
}
```
## License

© 2022 Wefi, LLC. All Rights Reserved.

